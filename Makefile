objects = analyze.o cut.o glue.o main.o listadna.o recombine.o

DNACrack: $(objects)
	gcc $(objects) -g -o DNACrack -Wall

analyze.o: analyze.c analyze.h
	gcc -c analyze.c -Wall

cut.o: cut.c cut.h
	gcc -c cut.c -Wall

glue.o: glue.c glue.h
	gcc -c glue.c -Wall

listadna.o: listadna.c listadna.h
	gcc -c listadna.c -Wall

main.o: main.c
	gcc -c main.c -Wall

recombine.o: recombine.c recombine.h
	gcc -c recombine.c -Wall
