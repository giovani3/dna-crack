//////////////////////////////////////////////////////////////////////
///// 						D N A  C R A C K					//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////	conteúdo desse arquivo: IMPLEMENTAÇÃO DAS FUNÇÕES DNA	//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////			implementado por Giovani G. Marciniak			//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////			Fique à vontade. Não repare a bagunça.			//////
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "analyze.h"

int dnaAnalyze(int argc, char **argv, int num) {
//-------------------- CRIANDO PONTEIRO ARQUIVOS DA ENTRADA --------------------//
/* Cria um ponteiro para cada arquivo na entrada. */
	FILE *fileProteina = NULL ; 
	FILE *fileGene = NULL ;
	if (argc < num+2) {
		printf("Erro nos arquivos de entrada\n");
		return 1;
	}																
	fileGene = fopen(argv[num+1], "r") ;
	fileProteina = fopen(argv[num+2], "r") ;

//----------------- CONTA QUANTOS CARACTERES TEM NOS ARQUIVOS -----------------//
	int contadorGene = 0 ;
	char x ;
	while ( ( x = fgetc(fileGene) ) != EOF) contadorGene++ ; 							
	int contadorProteina = 0;							
	while ((x = fgetc(fileProteina)) != EOF) contadorProteina++ ;
	if (contadorProteina > 3*contadorGene) erro() ;	

//-------------------- COLOCANDO OS ARQUIVOS EM UMA STRING --------------------//
	char *gene = (char*) malloc(contadorGene*(sizeof(char))) ;
	char *proteina = (char*) malloc(contadorProteina* (sizeof(char))) ;
	rewind(fileGene);
	rewind(fileProteina);
	
	fscanf(fileGene, "%s", gene);
	fscanf(fileProteina, "%s", proteina);
	//printf("%s\n%s\n",gene, proteina);

//---------------------- CRIA A TABELA DE CODIFICAÇÃO ----------------------//
	codificacao_t *tabela_codificacao = (codificacao_t*) malloc(64*sizeof(tabela_codificacao));
	cria_tabela(tabela_codificacao);

//---------------------- COMEÇA A TENTAR ACHAR POSIÇÃO ----------------------//
	int k = 0, full = 0, casamento = -1;
	for (int j = 0; j < (contadorGene - contadorProteina); j++) { //MUDEI AQUI
		if (full == 1) break;
		k = j + k;
		for (int i = 0; i < (contadorProteina-1); i++) {
			if ((tabela_codificacao[char_to_index(tabela_codificacao, gene[k], gene[k+1], gene[k+2])].aminoacido == '.')
			|| tabela_codificacao[char_to_index(tabela_codificacao, gene[k], gene[k+1], gene[k+2])].aminoacido == proteina[i]) //MUDEI AQUI
			{
				tabela_codificacao[char_to_index(tabela_codificacao, gene[k], gene[k+1], gene[k+2])].aminoacido = proteina[i];
				k = k + 3;
				if (i == contadorProteina-2) {
					full = 1;
					casamento = j;
				}
			}
			else {
				limpa_tabela(tabela_codificacao);
				k = 0;
				break;
			}
		}
	}
	if (casamento == -1) {printf("Casamento não encontrado\n"); return 1;};

	printf("Casamento encontrado na posicao %d\n", casamento);
	imprime_tabela(tabela_codificacao);
	return (casamento);
//--------------------------- FECHA OS ARQUIVOS ---------------------------//
	fclose(fileGene) ;
	fclose(fileProteina) ;
}


//############################### END #######################################//

//--------------------------- FUNÇÃO CRIA TABELA ---------------------------//
void cria_tabela(codificacao_t *tabela) { // eu ja recebi o endereço do primeiro valor de v
	int cont = 0;
	int k = 0, i = 0, j = 0;
	char bases[] = "ACTG";

	while (cont < 64) {
		tabela[cont].aminoacido = '.';
		tabela[cont].gene[0] = bases[k];
		tabela[cont].gene[1] = bases[i];
		tabela[cont].gene[2] = bases[j];

		if (((cont + 1) % 16) == 0) k++;
		if (((cont + 1) % 4) == 0) i = (i + 1) % 4;
		j = (j+1) % 4;
		cont++;
	}
}

//--------------------------- FUNÇÃO LIMPA TABELA ---------------------------//
void limpa_tabela(codificacao_t *tabela) {
		int cont = 0;
		while (cont < 64) {
			tabela[cont].aminoacido = '.';
			cont++;
		}
}

//--------------------------- FUNÇÃO IMPRIME TABELA ---------------------------//
void imprime_tabela(codificacao_t *tabela) {
	int g = 0;
	for (g = 0; g < 64; g++) {
		printf("%c", tabela[g].gene[0]);
		printf("%c", tabela[g].gene[1]);
		printf("%c ", tabela[g].gene[2]);
 		printf("%c    ", tabela[g].aminoacido);
 		int f = g + 1;
 		if (f%4 == 0) printf("\n");
 	} 
}

//--------------------------- FUNÇÃO CHAR TO INDEX ---------------------------//
int char_to_index(codificacao_t *tabela, char k0, char k1, char k2) {
	for (int i = 0; i < 64; i++) {
		if ((k0 == tabela[i].gene[0]) && (k1 == tabela[i].gene[1]) && (k2 == tabela[i].gene[2]) ) {
		//	printf("gene = %c%c%c\n indice = %d",k0, k1, k2, i);
			return i;	
		}
	} 
	return -1;
}

//--------------------------- FUNÇÃO HELP ---------------------------//
void help() {
	printf("	ARGUMENTO 			DESCRIÇÃO\n") ;
	printf("	-analyze 			Analiza genes e mostra os casamentos possíveis com a proteina.\n") ;
	printf("	-cut 				Corta uma cadeia de DNA para um arquivo de saída.\n") ;
	printf("	-help 				Mostra uma mensagem de ajuda.\n") ;
}

//--------------------------- FUNÇÃO ERRO ---------------------------//
void erro() {
	printf ("Alguma coisa deu errado.") ;
}