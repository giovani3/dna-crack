//////////////////////////////////////////////////////////////////////
///// 						D N A  C R A C K					//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////	conteúdo desse arquivo: IMPLEMENTAÇÃO DAS FUNÇÕES CUT	//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////			implementado por Giovani G. Marciniak			//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////			Fique à vontade. Não repare a bagunça.			//////
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "listadna.h"
#include "cut.h"

void dnaCut(int argc, char **argv, int linear) {
//-------------------- CRIANDO PONTEIRO ARQUIVOS DA ENTRADA --------------------//
	if (!linear)
	{
		printf("Cadeia não-linear!\n");
	}																								// Testa se a cadeia é circular ou linear

	if (argc < 4) 
	{
		printf("Erro nos arquivos de entrada\n");
		return;
	}																								// Verifica se tem entradas suficientes

	FILE *fileDna = NULL ; 																			// Cria ponteiros para arquivos vazios
	FILE *fileEnzima = NULL ;

	fileDna = fopen(argv[3], "r") ;
	fileEnzima = fopen(argv[4], "r") ;																// Lê a partir das entradas no terminal

//----------------- CONTA QUANTOS CARACTERES TEM NOS ARQUIVOS -----------------//
	int contadorDna = contadorArquivo (fileDna);						
	int contadorEnzima = contadorArquivo(fileEnzima);							

	if (contadorEnzima > contadorDna) {printf("Erro nos arquivos de entrada.\n"); return;}

//-------------------- COLOCANDO OS ARQUIVOS ~DNA~ EM UMA CADEIA --------------------//
	CadeiaDNA *cadeia = novaCadeia();
	criaCadeia(cadeia,'l');
	insertFileChain(fileDna, cadeia);
	//imprimeCadeia(cadeia);

//-------------------- COLOCANDO OS ARQUIVOS ~ENZIMA~ EM UMA CADEIA --------------------//
	CadeiaDNA *enzima = novaCadeia();
	criaCadeia(enzima,'l');
	insertFileChain(fileEnzima, enzima);
	//imprimeCadeia(enzima);

//-------------------- FECHANDO OS ARQUIVOS --------------------//	
	fclose(fileDna);
	fclose(fileEnzima);

//-------------------- ENCONTRA AS PONTAS CEGAS/ADESIVAS - LISTA VERSION --------------------//
	TipoApontador aux = cadeia->primeiro->prox;

	for (int i = 0; i < (contadorDna - contadorEnzima +1); i++) {
		if (!comparaListas(enzima,aux)) {
			aux->item.situacao = Cega;
			aux->item.base += 32; 						 				//MINISCULO

			TipoApontador aux2 = aux->prox;
			for (int j = 1; j < contadorEnzima; j++) {
				aux2->item.situacao = Adesiva;
				aux2->item.base += 32;									//MINISCULO
				aux2 = aux2->prox;
			}
			aux = aux->prox;

			if (!linear) goto para_antes;
		}
		else
			aux = aux->prox;
	}

	para_antes: ;

//-------------------- CRIA A LISTA DE CADEIA --------------------//
	Lista *lista = novaLista();
	criaLista(lista);
	insereLista(cadeia, lista); 

//-------------------- CORTA A LISTA DE CADEIA --------------------//
	aux = cadeia->primeiro->prox;
	while (aux != NULL) {
		if (aux->item.situacao == Cega) {
			insereLista(corta_e_devolve(aux, cadeia), lista);
			aux = lista->ultimo->item.primeiro->prox;
		}
		else
			aux = aux->prox;
	}

	//-------------------- IMPRIME LISTA NO ARQUIVO --------------------//
	if (linear) {
		char *saida = (char*) malloc (contadorDna*sizeof(char));
		FILE *fileSaida = fopen("saida.txt", "w+");
		TipoApontadorL auxL = lista->primeiro->prox;
		while (auxL != NULL) {
			imprimeCadeia_to_string(&auxL->item, saida);
			fprintf(fileSaida, "%s", saida);
			auxL = auxL->prox;
		}
		fclose(fileSaida);
		printf("Corte gravado no arquivo \"saida.txt\".\n");
	}

	else {
		char *saida = (char*) malloc (contadorDna*sizeof(char));
		FILE *fileSaida = fopen("saidacircular.txt", "w+");
		TipoApontadorL auxL = lista->primeiro->prox;
		while (auxL != NULL) {
			imprimeCadeia_to_string(&auxL->item, saida);
			fprintf(fileSaida, "%s", saida);
			auxL = auxL->prox;
		}
		fclose(fileSaida);
		printf("Corte gravado no arquivo \"saidacircular.txt\".\n");
	}
	return;
}

//////////////////////////////////////////////////////////////////////////
//								sub-funcoes								//
//////////////////////////////////////////////////////////////////////////

void insertFileChain(FILE *arq, CadeiaDNA *cadeia) {
	TipoItem a; char x;
	rewind(arq);
	while ( ( x = fgetc(arq) ) != EOF) {
		a.base = x;
		a.situacao = Normal;
		insereCadeia(a,cadeia);
	}
}

int contadorArquivo (FILE *arq) {
	char x; int contador = 0;
	rewind(arq);
	while ( ( x = fgetc(arq) ) != EOF) { contador++ ;} 
	return contador;
}

CadeiaDNA* corta_e_devolve(TipoApontador aux, CadeiaDNA *cadeia) {
	CadeiaDNA *cadeiaRetorno = novaCadeia();
	criaCadeia(cadeiaRetorno, 'l');
	cadeiaRetorno->ultimo = cadeia->ultimo;
	cadeiaRetorno->ultimo->prox = NULL;
	cadeiaRetorno->primeiro->prox = aux->prox;

	cadeia->ultimo = aux;
	cadeia->ultimo->prox = NULL;
	return cadeiaRetorno;
}

int comparaListas(CadeiaDNA *enzima,TipoApontador auxLista)
{
	TipoApontador auxEnzima = enzima->primeiro->prox;
	while ((auxEnzima != NULL) && (auxLista != NULL)) {
		if (auxEnzima->item.base != auxLista->item.base) {
			return 1;
		}
		auxLista = auxLista->prox;
		auxEnzima = auxEnzima->prox;
	}
	return 0;
}