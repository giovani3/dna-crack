#ifndef __CUT__
#define __CUT__

void dnaCut(int argc, char **argv, int linear);
CadeiaDNA* corta_e_devolve(TipoApontador aux, CadeiaDNA *cadeia);
void imprimeCadeia_to_string (CadeiaDNA *l, char *x);
int comparaListas(CadeiaDNA *enzima,TipoApontador auxLista);
void insertFileChain(FILE *arq, CadeiaDNA *cadeia);
int contadorArquivo (FILE *arq);
void dnaCutCircular(int argc, char **argv);

#endif