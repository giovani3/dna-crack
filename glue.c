#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "listadna.h"
#include "cut.h"
#include "glue.h"

void dnaGlue(int argc, char **argv, int linear) {
	if (linear != 1) {
		printf("Cadeias não-lineares não são suportadas na versão beta.\n");
		return;
	}										// Testa se a cadeia é circular ou linear

	if (argc < 5) {
		printf("Erro nos arquivos de entrada.\n");
		return;
	}										// Verifica se tem entradas suficientes

	FILE *fileDna1 = NULL ; 				// Cria ponteiros para arquivos vazios
	FILE *fileDna2 = NULL ;

	fileDna1 = fopen(argv[3], "r") ;
	fileDna2 = fopen(argv[4], "r") ;		// Lê a partir das entradas no terminal
					

	Lista *dna1 = novaLista();				// Cria nova lista de listas vazia	dna1								
	criaLista(dna1);
	insereArqLista(fileDna1, dna1);

	Lista *dna2 = novaLista();				// 				|| 		||			dna 2
	criaLista(dna2);
	insereArqLista(fileDna2, dna2); 

	fclose(fileDna1);						// fecha arquivos
	fclose(fileDna2);

	if (colaCadeias(dna1, dna2)) {			// só se colar imprime a cadeia maior
		// Impressão
		Lista *maior = novaLista();
		criaLista(maior);
		insereLista(maiorCadeia(dna1), maior);
		listaParaArquivo (maior, "output.txt");
	}

	juntaDuasListas(dna1, dna2);			// junta as duas para colcoar em um arquivo
	listaParaArquivo (dna1, "dump.txt");

}

////////////// subrotinas

void juntaDuasListas(Lista *dna1, Lista *dna2) 
{
	dna1->ultimo->prox = dna2->primeiro->prox;
	dna1->ultimo = dna2->ultimo;
	free(dna2);
}

int colaCadeias (Lista *dna1, Lista *dna2)
{
//	TipoApontadorL guardaDNA1 = dna1->primeiro->prox;
	TipoApontadorL auxDNA01_1 = NULL;
	TipoApontadorL auxDNA01_2 = NULL;
	TipoApontadorL auxDNA02 = dna2->primeiro->prox;
	int quebra = 0;
	int boolean_casou = 0;

	while (auxDNA02 != NULL) { 													// vai até o fim do dna 2

		auxDNA01_1 = dna1->primeiro->prox;

		while ((auxDNA01_1->prox != NULL)&&(!quebra)) {
			auxDNA01_2 = auxDNA01_1->prox;

			while ((auxDNA01_2 != NULL)&&(!quebra)) {
				if (casou(&auxDNA02->item, &auxDNA01_1->item, &auxDNA01_2->item)) {
					auxDNA01_2 = auxDNA01_2->ant;
					cola(&auxDNA02->item, auxDNA01_1, auxDNA01_2->prox, dna1); 	// faz a cola
					auxDNA02 = auxDNA02->ant;
					removeListaSemFree(auxDNA02->prox, dna2); 					// remove da lista sem liberar todos, só a  cabeça
					boolean_casou = quebra = 1; 								// se casou pode quebrar
					break;
				}

				else if (casou(&auxDNA02->item, &auxDNA01_2->item, &auxDNA01_1->item)) {
					printf("INVERTIDO!\n");										
					auxDNA01_1 = auxDNA01_1->ant;
					colaInvertido(&auxDNA02->item, auxDNA01_2, auxDNA01_1->prox, dna1);
					auxDNA02 = auxDNA02->ant;
					removeListaSemFree(auxDNA02->prox, dna2);
					boolean_casou = quebra = 1;
					break;
				}

				auxDNA01_2 = auxDNA01_2->prox;
			}
			auxDNA01_1 = auxDNA01_1->prox;
			
		}
		auxDNA02 = auxDNA02->prox;
		quebra = 0;
	}

	auxDNA01_1 = dna1->primeiro->prox;
		while (auxDNA01_1 != NULL) {
			auxDNA02 = dna2->primeiro->prox;
			while (auxDNA02 != NULL) {
				if (casou(&auxDNA02->item, &auxDNA01_1->item, &auxDNA01_1->item)) {
					printf("COLOU UMA CIRCULAR!!!\n");
					colaCircular(&auxDNA02->item, auxDNA01_1, dna1);
					imprimeCadeiaCircular (&auxDNA01_1->item);
					removeListaSemFree(auxDNA02, dna2);
					boolean_casou = 1;
					break;
				}
				auxDNA02 = auxDNA02->prox;
			}
			auxDNA01_1 = auxDNA01_1->prox;
		}

	return boolean_casou;
}

int casou (CadeiaDNA *cadeia_do_meio, CadeiaDNA *cadeia_lateral01, CadeiaDNA *cadeia_lateral02)
{
	if ((cadeia_do_meio->primeiro->prox->item.situacao != Adesiva) || (cadeia_do_meio->ultimo->item.situacao != Cega)) {
		return 0;
	}

	if (cadeia_do_meio->ultimo->item.base != cadeia_lateral01->ultimo->item.base) {
		return 0;
	}

	if (!comparaAdesivas(cadeia_do_meio, cadeia_lateral02)) {
		return 0;
	}

	printf("CASOU SIMPLES!\n");
	return 1;
}

int comparaAdesivas(CadeiaDNA *cadeia1, CadeiaDNA *cadeia2)
{
	TipoApontador aux1 = cadeia1->primeiro->prox;
	TipoApontador aux2 = cadeia2->primeiro->prox;

	while ((aux1->item.situacao == Adesiva) || (aux2->item.situacao == Adesiva)) {
		if (aux1->item.base != aux2->item.base) {
			return 0;
		}
		aux1 = aux1->prox;
		aux2 = aux2->prox;
	}
	return 1;
}

void cola (CadeiaDNA *cadeia_colar, TipoApontadorL lado_direito, TipoApontadorL lado_esquerdo, Lista *L)
{
//	printf("lado_direito oficial %d\n", lado_direito);
//	printf("lado esquerdo oficial %d\n", lado_esquerdo);
//	printf("lista %d\n", L->ultimo);
//	printf("lado_direito %d\n", lado_direito->item.ultimo); printf("eh aqui\n");
//	printf("lado esquerdo %d\n", lado_esquerdo->item.ultimo);

	lado_direito->item.ultimo->prox = cadeia_colar->primeiro->prox;
	lado_direito->item.ultimo->prox->ant = lado_direito->item.ultimo;
	lado_direito->item.ultimo = cadeia_colar->ultimo;

	lado_direito->item.ultimo->prox = lado_esquerdo->item.primeiro->prox;
	lado_direito->item.ultimo->prox->ant = lado_direito->item.ultimo;
	lado_direito->item.ultimo = lado_esquerdo->item.ultimo;

	lado_direito->item.tamanho += cadeia_colar->tamanho + lado_esquerdo->item.tamanho;

//	cadeia_colar->primeiro->prox = NULL;
//	cadeia_colar->primeiro->ant = NULL;


//	printf("lado_direito ultimo %d\n", lado_direito->item.ultimo);
//	printf("lado esquerdo ultimo %d\n", lado_esquerdo->item.ultimo);
//	printf("lista %d\n", L->ultimo);
	
	//free(lado_esquerdo);
	//removeListaSemFree(lado_esquerdo, L);
//	printf("lista %d\n", L->ultimo);
}

void colaInvertido (CadeiaDNA *cadeia_colar, TipoApontadorL lado_primeiro, TipoApontadorL lado_ultimoGUARDA, Lista *L)
{
	cadeia_colar->primeiro->prox->ant = lado_ultimoGUARDA->item.primeiro->prox;
	cadeia_colar->ultimo->prox = lado_ultimoGUARDA->item.primeiro->prox;
	lado_ultimoGUARDA->item.primeiro->prox = cadeia_colar->primeiro->prox;
	
	lado_primeiro->item.primeiro->prox->ant = lado_ultimoGUARDA->item.primeiro->prox;
	lado_primeiro->item.ultimo->prox = lado_ultimoGUARDA->item.primeiro->prox;
	lado_ultimoGUARDA->item.primeiro->prox = lado_primeiro->item.primeiro->prox;
	
	lado_ultimoGUARDA->item.tamanho += cadeia_colar->tamanho + lado_primeiro->item.tamanho;
	removeListaSemFree(lado_primeiro, L);
}

void colaCircular (CadeiaDNA *cadeia_colar, TipoApontadorL aux, Lista *L) {

	aux->item.tamanho += cadeia_colar->tamanho;
	aux->item.ultimo->prox = cadeia_colar->primeiro->prox;				// O proximo item da lista é o primeiro da cadeia
	cadeia_colar->ultimo->prox = aux->item.primeiro;					// O ultimo da cadeia é o primeiro da lista
	aux->item.primeiro->ant = cadeia_colar->ultimo;						// O anterior ao primeiro é o ultimo da cadeia
	aux->item.ultimo = cadeia_colar->ultimo;
	cadeia_colar->primeiro->ant = aux->item.ultimo;
	aux->item.estilo = 'c';
}

void insereArqLista(FILE *arq, Lista *l) {
	char x = 'x';
	rewind(arq);

	while (x != EOF) {
		insereLista (cadeiaAteEspaco(arq, &x), l);
	}		
}

CadeiaDNA* cadeiaAteEspaco (FILE *arq, char *x) {
	TipoItem a; int adesiva = 1;

	CadeiaDNA *cadeiadna = novaCadeia();
	criaCadeia(cadeiadna,'l');

	while ( (*x = fgetc(arq)) != ' ' && (*x != EOF) ) {			// quando nao termina o arquivo
		a.base = *x;
		if (a.base < 91) {									
			a.situacao = Normal;
			adesiva = 0;
		}

		else {													// se nao for adesiva ou cega
			if (!adesiva)
				a.situacao = Cega;
			else
				a.situacao = Adesiva;
		}

	insereCadeia(a, cadeiadna);
	}
	return cadeiadna;
}

CadeiaDNA* maiorCadeia (Lista *l)							// calcula a maior cadeia
{
	TipoApontadorL maior = l->primeiro->prox;	
	TipoApontadorL aux = maior->prox;

	while (aux != NULL) {
		if (maior->item.tamanho < aux->item.tamanho) 
			maior = aux;
		aux = aux->prox;
	}

	if (maior == l->ultimo) {							// se for o ultimo tem que atualizar
		l->ultimo = maior->ant;
		l->ultimo->prox = NULL;
	}

	else {
		maior->ant->prox = maior->prox;
		maior->prox->ant = maior->ant;
	}

	CadeiaDNA *retorno = &maior->item;				// retorna o maior item

	return retorno;
}