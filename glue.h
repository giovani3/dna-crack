#ifndef __GLUE__
#define __GLUE__

void dnaGlue(int argc, char **argv, int linear);
void insereArqLista(FILE *arq, Lista *l);
CadeiaDNA* cadeiaAteEspaco (FILE *arq, char *x);
int casou (CadeiaDNA *cadeia_do_meio, CadeiaDNA *cadeia_lateral01, CadeiaDNA *cadeia_lateral02);
void cola (CadeiaDNA *cadeia_colar, TipoApontadorL lado_direito, TipoApontadorL lado_esquerdo, Lista *L);
void colaInvertido (CadeiaDNA *cadeia_colar, TipoApontadorL lado_primeiro, TipoApontadorL lado_ultimoGUARDA, Lista *L);
void insereArqLista(FILE *arq, Lista *l);
CadeiaDNA* cadeiaAteEspaco (FILE *arq, char *x);
int comparaAdesivas(CadeiaDNA *cadeia1, CadeiaDNA *cadeia2);
void colaCircular (CadeiaDNA *cadeia_colar, TipoApontadorL aux, Lista *L);
void juntaDuasListas(Lista *dna1, Lista *dna2);
int colaCadeias (Lista *dna1, Lista *dna2);
CadeiaDNA* maiorCadeia (Lista *l);
int casouCircular (CadeiaDNA *cadeia_do_meio, CadeiaDNA *cadeia_lateral) ;

#endif