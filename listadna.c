#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "listadna.h"

//-------------- APIs DA LISTA CHAMADA CADEIA ------------------
CadeiaDNA* novaCadeia (){
	CadeiaDNA *cadeia = malloc(sizeof(CadeiaDNA));
	cadeia->primeiro = cadeia->ultimo = NULL;
	cadeia->estilo = '.';
	return cadeia;
}

void criaCadeia (CadeiaDNA *l, char style) {
	l->primeiro=(TipoApontador) malloc(sizeof(TipoCelula));
	l->ultimo = l->primeiro;
	l->primeiro->ant = NULL;
	l->primeiro->prox = NULL;
	l->estilo = style;
	l->tamanho = 0;
}

void insereCadeia (TipoItem x, CadeiaDNA *l) {
	l->ultimo->prox = (TipoApontador) malloc (sizeof(TipoCelula));
	l->ultimo->prox->ant = l->ultimo;
	l->ultimo = l->ultimo->prox;
	l->ultimo->item = x;
	l->ultimo->prox = NULL;
	l->tamanho++;
}

void imprimeCadeia (CadeiaDNA *l) {
	TipoApontador aux;
	aux = l->primeiro->prox;
	while (aux != NULL) {
		printf("%c", aux->item.base);
		aux = aux->prox;
	}
	printf("\n");
}

void imprimeCadeiaCircular (CadeiaDNA *l)
{
	TipoApontador aux;
	aux = l->primeiro->prox;
	while (aux->ant != l->ultimo) {
		printf("%c", aux->item.base);
		aux = aux->prox;
	}
	printf("\n");
} 

void imprimeCadeiaS (CadeiaDNA *l) {
	TipoApontador aux;
	aux = l->primeiro->prox;
	while (aux != NULL) {
		printf("%d", aux->item.situacao);
		aux = aux->prox;
	}
//	printf("\n");
	printf(" ");
}

void imprimeCadeia_to_string (CadeiaDNA *l, char *x) {
	TipoApontador aux;
	aux = l->primeiro->prox;
	int i = 0;

	if (l->estilo == 'c') {
		while (aux->ant != l->ultimo) {
			x[i] = aux->item.base;
			aux = aux->prox;
			i++;
	}

	}
	else {
		while (aux != NULL) {
			x[i] = aux->item.base;
			aux = aux->prox;
			i++;
		}
	}
	x[i] = ' '; i++;
	x[i] = '\0';
	
}

void imprimeCadeia_to_stringS (CadeiaDNA *l, char *x) {
	TipoApontador aux;
	aux = l->primeiro->prox;
	int i = 0;

	if (l->estilo == 'c') {
		while (aux->ant != l->ultimo) {
			x[i] = (char) aux->item.situacao;
			aux = aux->prox;
			i++;
	}

	}
	else {
		while (aux != NULL) {
			x[i] = (char)aux->item.situacao;
			aux = aux->prox;
			i++;
		}
	}
	x[i] = ' '; i++;
	x[i] = '\0';
	
}

///////////////////////////////////////////////////////////////
//-------------- APIs DA LISTA CHAMADA LISTA ------------------
///////////////////////////////////////////////////////////////


Lista* novaLista (){
	Lista *listacadeia = malloc(sizeof(Lista));
	listacadeia->primeiro = listacadeia->ultimo = NULL;
	listacadeia->quantidade = 0;
	return listacadeia;
}

void criaLista (Lista *L) {
	L->primeiro =(TipoApontadorL) malloc(sizeof(TipoCelulaL));
	L->ultimo = L->primeiro;
	L->primeiro->ant = NULL;
	L->primeiro->prox = NULL;
	L->quantidade = 0;
}

int listaVazia(Lista *L)
{
	return (L->primeiro == L->ultimo);
}

void insereLista (CadeiaDNA *x, Lista *L) {
	L->ultimo->prox = (TipoApontadorL) malloc (sizeof(TipoCelulaL));
	L->ultimo->prox->ant = L->ultimo;
	L->ultimo = L->ultimo->prox;
	L->ultimo->item = *x;
	L->ultimo->prox = NULL;
	L->quantidade++;
}

void removeListaSemFree (TipoApontadorL q, Lista *L)
{
	if (q == NULL) {
		printf("TÁ PEGANDO FOGO BICHO! Apontador tá apontando pra NULL, MEU!!!!\n");
		return;
	}

	if (!(L->ultimo == q)) {
		q->ant->prox = q->prox;
		q->prox->ant = q->ant;
	}
	else {
		q->ant->prox = NULL;
		L->ultimo = q->ant;
	}
	free(q);

}

void listaParaArquivo (Lista *lista, const char *str) 
{
	char *saida;
	FILE *fileSaida = fopen(str, "w+");
	TipoApontadorL auxL = lista->primeiro->prox;
	while (auxL != NULL) {
		saida = (char*) malloc (auxL->item.tamanho*sizeof(char));
		imprimeCadeia_to_string(&auxL->item, saida);
		fprintf(fileSaida, "%s", saida);
		auxL = auxL->prox;
		free(saida);
	}
	fclose(fileSaida);
	printf("Cadeia(s) gravada(s) no arquivo \"%s\".\n", str);
}

void imprimeLista (Lista *lista)
{
	char *saida;
	TipoApontadorL auxL = lista->primeiro->prox;
	while (auxL != NULL) {
		saida = (char*) malloc (auxL->item.tamanho*sizeof(char));
		imprimeCadeia_to_string(&auxL->item, saida);
		printf("%s", saida);
		auxL = auxL->prox;
		free(saida);
	}
}

void imprimeListaS (Lista *lista)
{
	TipoApontadorL auxL = lista->primeiro->prox;
	while (auxL != NULL) {
		imprimeCadeiaS(&auxL->item);
		auxL = auxL->prox;
	}
}