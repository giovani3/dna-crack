#ifndef __LISTADNA__
#define __LISTADNA__

//--------------------------------//
//----- Sobre a "CadeiaDNA" ------//
//--------------------------------//

typedef enum {
	Normal,
	Adesiva,
	Cega,
} TipoSituacao;

typedef struct TipoItem {
	char base;						// A, C, T ou G
	TipoSituacao situacao;			// Normal, Adesiva, Cega
} TipoItem;

typedef struct TipoCelula *TipoApontador;

typedef struct TipoCelula {
	TipoItem item;
	TipoApontador ant, prox;
} TipoCelula;

typedef struct CadeiaDNA {
	TipoApontador primeiro, ultimo;
	char estilo; 	//'c' para circular, 'l' para linear
	int tamanho;	// tamanho da cadeia
} CadeiaDNA;

//------------------------------//
//---- Manipulação da Cadeia----//
//------------------------------//
void criaCadeia (CadeiaDNA *l, char style);

void insereCadeia (TipoItem x, CadeiaDNA *l);

CadeiaDNA* novaCadeia ();

void imprimeCadeia (CadeiaDNA *l);
void imprimeCadeiaS (CadeiaDNA *l);
void imprimeCadeiaCircular (CadeiaDNA *l);

///////////////////////////////////////////////////
//---- Agora sobre a "Lista" 
////////////////////////////////////////////////////

typedef struct TipoCelulaL *TipoApontadorL;

typedef struct TipoCelulaL {
	CadeiaDNA item;
	TipoApontadorL ant, prox;
} TipoCelulaL;

typedef struct Lista {
	TipoApontadorL primeiro, ultimo;
	int quantidade;
} Lista;

//------------------------------//
//---- Manipulação da Lista ----//
//------------------------------//
Lista* novaLista();
void criaLista(Lista *L);
void insereLista(CadeiaDNA *x, Lista *L);
void removeListaSemFree (TipoApontadorL q, Lista *L);
int listaVazia(Lista *L);
void listaParaArquivo (Lista *lista, const char *str);
void imprimeLista (Lista *lista);
void imprimeListaS (Lista *lista);

#endif