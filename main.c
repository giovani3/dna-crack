//////////////////////////////////////////////////////////////////////
///// 					D N A  C R A C K  v.1.1					//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////		conteúdo desse arquivo: PROGRAMA PRINCIPAL			//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////			implementado por Giovani G. Marciniak			//////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/////		Entre e fique à vontade. Não repare a bagunça.		//////
//////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "analyze.h"
#include "listadna.h"
#include "cut.h"
#include "glue.h"
#include "recombine.h"

int main (int argc, char **argv) {

	int invalido = 1 ;										/* Cai na mensagem padrão. */
	int linear = 0 ;

	for (int i = 1; i < argc; i++) {						/* Procura os argumentos na entrada na função */
		if (!strcmp(argv[i], "-analyze")) {
			printf("Opção Selecionada: DNA ANALYZE\n") ;
			invalido = 0 ;									/* Não vai mais cair na mensagem padrão. */
			dnaAnalyze(argc, argv, i) ;
			break;
		}

		else if (!strcmp(argv[i], "-cut")) {
			printf("Opção Selecionada: DNA CUT\n");
			for (int i = 1; i < argc; i++) if (strcmp(argv[i], "-l") == 0) linear = 1;
			invalido = 0 ;
			dnaCut(argc, argv, linear);
			break;
		}

		else if (!strcmp(argv[i], "-glue")) {
			printf("Opção Selecionada: DNA GLUE\n");
			for (int i = 1; i < argc; i++) if (!strcmp(argv[i], "-l")) linear = 1;
			invalido = 0 ;
			dnaGlue(argc, argv, linear);
			break;
		}

		else if (!strcmp(argv[i], "-recombine")) {
			printf("Opção Selecionada: DNA RECOMBINE\n");
			for (int i = 1; i < argc; i++) if (!strcmp(argv[i], "-l")) linear = 1;
			invalido = 0 ;
			dnaRecombine(argc, argv, linear);
			break;
		}

		else if (!strcmp(argv[i], "-help")) {
			printf("Opção Selecionada: HELP\n");
			invalido = 0 ;
			help();
			break;
		}
	}

	/* Mensagem padrão */
	if (invalido)
		printf("Opção Inválida. Tente \"-help\" para conhecer as opções disponíveis.\n") ;

	return (0);
}
