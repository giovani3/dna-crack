#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "listadna.h"
#include "cut.h"
#include "recombine.h"
#include "glue.h"

void dnaRecombine(int argc, char **argv, int linear)
{
		if (linear) {
		printf("Cadeias lineares não são suportadas na versão beta.\n");
		return;
	}																			// Testa se a cadeia é circular ou linear

	if (argc < 5) {
		printf("Erro nos arquivos de entrada.\n");
		return;
	}																			// Verifica se tem entradas suficientes

	// Lê a partir das entradas no terminal

	char* guarda = argv[4];
	argv[4] = argv[5];
	dnaCut(argc-1, argv, 0);
	argv[3] = guarda;
	dnaCut(argc-1, argv, 1);

	FILE *fileDna1Circular = fopen("saidacircular.txt", "r");					// Cria ponteiros para arquivos vazios
	FILE *fileDna2Linear = fopen("saida.txt", "r");			

	Lista *dna1Circular = novaLista();
	criaLista(dna1Circular);
	insereArqLista(fileDna1Circular, dna1Circular);

	Lista *dna2Linear = novaLista();										// Cria listas
	criaLista(dna2Linear);
	insereArqLista(fileDna2Linear, dna2Linear); 

	fclose(fileDna1Circular);
	fclose(fileDna2Linear);

	if (dna1Circular->quantidade < 3) {
		printf("Enzima não cortou!\n");
		return;
	}

/*//// qualquer coisa apaga aqui
	if (dna1Circular->primeiro->prox->prox == NULL) {
		dna1Circular->primeiro->prox->item.estilo = 'c';
		dna1Circular->primeiro->prox->item.primeiro->ant = dna1Circular->primeiro->prox->item.ultimo;
		dna1Circular->primeiro->prox->item.ultimo->prox = dna1Circular->primeiro->prox->item.primeiro;
		imprimeLista(dna1Circular);
		//tentaEnzimaCircularNaPonta(dna1Circular,argv[4]);

	}

/// fim qualquer coisa apaga aqui
*/


	TipoApontadorL aux1 = dna1Circular->primeiro->prox;		
	TipoApontadorL aux2 = dna2Linear->primeiro->prox;

	while (aux2 != NULL) {
		if (casou(&aux2->item, &aux1->item, &aux1->prox->item)) {	// tenta fazer os casamentos
			cola(&aux2->item, aux1, aux1->prox, dna1Circular);
			removeListaSemFree(aux2, dna2Linear);	
			goto quebra_dois;										// sai do while
		}
		aux2 = aux2->prox;
	}
	quebra_dois: ;

	aux1 = dna1Circular->primeiro->prox->prox;					// limpa restantes
	while (dna1Circular->primeiro->prox->prox != NULL) {
		removeListaSemFree(aux1, dna1Circular);
		aux1 = dna1Circular->primeiro->prox->prox;
	}

	listaParaArquivo (dna1Circular, "saida_recombine.txt");		// imprime em saida_recombine

}